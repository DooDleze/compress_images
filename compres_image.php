<?php

if(!$_POST['forcestop']){

	if($_POST['start-compress']){

		setcookie("folder", $_POST['folder'], time()+360000);
		setcookie("resize", $_POST['resize'], time()+360000);
		setcookie("quality", (int)$_POST['quality'], time()+360000);
		setcookie("maxwidth", (int)$_POST['maxwidth'], time()+360000);
		setcookie("maxheight", (int)$_POST['maxheight'], time()+360000);
		setcookie("extension", $_POST['extension'], time()+360000);

		if($_POST['folder']==''){
			$returnData = [
				'success' => false,
				'continue' => false,
				'alertMessage' => 'Укажите папку!',
			];
			echo json_encode($returnData);
			die();
		}
		

		if(isset($_POST['resize'])){
			$res=true;
		}

		$path_to_folder=$_SERVER['DOCUMENT_ROOT'].$_POST['folder'];

		$quality=($_POST['quality']!=''?(int)$_POST['quality']:70);
		$maxwidth=($_POST['maxwidth']!=''?(int)$_POST['maxwidth']:1500);
		$maxheight=($_POST['maxheight']!=''?(int)$_POST['maxheight']:1500);

		if($_POST['extension']!=''){
			$extension = explode(',',$_POST['extension']);
		}else{
			$extension=['jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF'];
		}
		
		$log = [];

		$ALLDIRS = [];

		$continueDirs = json_decode( file_get_contents($_SERVER['DOCUMENT_ROOT'].'/compress_image_temp.txt'), 1 );

		function addDirToCache($dirsArray){
			file_put_contents($_SERVER['DOCUMENT_ROOT'].'/compress_image_dirs.txt',json_encode($dirsArray));
		}

		function addPathToTemp($path){
			$tempDirs = json_decode( file_get_contents($_SERVER['DOCUMENT_ROOT'].'/compress_image_temp.txt'), 1 );
			$tempDirs[$path] = $path;
			file_put_contents($_SERVER['DOCUMENT_ROOT'].'/compress_image_temp.txt',json_encode($tempDirs));
		}

		
		Function GetDirsList($dir)
		{
			global $ALLDIRS;
			$ListDir = Array();
			If ($handle = opendir($dir))
			{
				While (False !== ($file = readdir($handle)))
				{
					If ($file == '.' || $file == '..')
					{
						continue;
					}
					$path = $dir  .'/'. $file;

					If(Is_File($path))
					{
						continue;
					}
					ElseIf(Is_Dir($path))
					{
						$ALLDIRS[] = $path;
						$ListDir = array_merge($ListDir, GetDirsList($path));
						// $ALLDIRS = array_merge($ALLDIRS, $ListDir);
					}
				}
				CloseDir($handle);
				Return $ListDir;
			}
		}

		
		Function GetFilesArr($dir)
		{
			global $extension;
			global $quality;
			global $maxwidth;
			global $maxheight;
			global $res;
			global $log;

			$ListDir = Array();
			If ($handle = opendir($dir))
			{
				While (False !== ($file = readdir($handle)))
				{
					If ($file == '.' || $file == '..')
					{
						continue;
					}
					$path = $dir  .'/'. $file;

					If(Is_File($path))
					{
						$ListDir[] = $path;

						$path_parts = pathinfo($path);
						// echo $path_parts['dirname'], "\n";
						// echo $path_parts['basename'], "\n";
						// echo $path_parts['extension'], "\n";

						if(in_array($path_parts['extension'], $extension)){
				
							try {
								$im = new imagick($path);
								$log[] = 'обрабатываю изображение: '.$path;
								$im->setImageCompression(Imagick::COMPRESSION_JPEG);
								$im->setImageCompressionQuality($quality);
								
								if($res){
									
									$d = $im->getImageGeometry(); 
									$w = $d['width']; 
									$h = $d['height'];
									$log[] = 'Текущий размер '.$w.' - '.$h;
									if($w>$maxwidth OR $h>$maxheight){

										$log[] = 'Обрезка '.$path;
										$im->scaleImage($maxwidth, $maxheight, true);
										$d = $im->getImageGeometry(); 
										$w = $d['width']; 
										$h = $d['height'];
										$log[] = 'Размер после обрезки - '.$w.' - '.$h;
									}else{
										$log[] = 'Пропускаю - '.$path;
									}
								}

								$im->writeImage($path_parts['dirname'].'/'.$path_parts['basename']); 
								$im->clear(); 
								$im->destroy();	

							} catch (Exception $e) {
								$log[] = 'Выброшено исключение: '.$e->getMessage().'<br>';
							}
				
							
						}
						

					}
					ElseIf(Is_Dir($path))
					{
						$ListDir= array_merge($ListDir, GetFilesArr($path));
					
						/* global $continueDirs;
						if( in_array( $path, $continueDirs) ){
							// Пропускаю обработанную папку
							$log[] = 'Пропускаю обработанную папку: '.$path.'<br>';
							// Continue;
						}else{
							addPathToTemp($path);
							$log[] = 'Обрабатываю папку: '.$path.'<br>';
							$ListDir= array_merge($ListDir, GetFilesArr($path));

							$showlog = implode('<br>',$log);
							$returnData = [
								'success' => true,
								'continue' => true,
								'log' => $showlog
							];
							echo json_encode($returnData);
							die();
						} */
					}
				}
				CloseDir($handle);
				Return $ListDir;
			}
		}

		
		
		$ALLDIRS = json_decode( file_get_contents($_SERVER['DOCUMENT_ROOT'].'/compress_image_dirs.txt'), 1 );
		if(!count($ALLDIRS)){
			GetDirsList($path_to_folder);
			$ALLDIRS[] = $path_to_folder;
			addDirToCache($ALLDIRS);
		}

		asort($ALLDIRS);

		foreach ($ALLDIRS as $dir) {

			if( in_array( $dir, $continueDirs) ){
				// Пропускаю обработанную папку
				// $log[] = 'Пропускаю обработанную папку: '.$dir.'<br>';
				// Continue;
			}else{
				$log[] = 'Обрабатываю папку: '.$dir.'<br>';
				addPathToTemp($dir);
				GetFilesArr($dir);
				$showlog = implode('<br>',$log);
				$returnData = [
					'success' => true,
					'continue' => true,
					'log' => $showlog,
				];
				echo json_encode($returnData);
				die();
			}
		}

		$showlog = implode('<br>',$log);
		$returnData = [
			'success' => true,
			'continue' => false,
			'log' => $showlog
		];
		echo json_encode($returnData);
		die();

	}

}else{
	$returnData = [
		'success' => true,
		'continue' => false,
		'log' => ''
	];
	echo json_encode($returnData);
	die();
}

if($_POST['clearcache']){
	unlink($_SERVER['DOCUMENT_ROOT'].'/compress_image_temp.txt');
	unlink($_SERVER['DOCUMENT_ROOT'].'/compress_image_dirs.txt');

	$returnData = [
		'success' => true,
		'alertMessage' => 'Кэш очищен',
	];

	echo json_encode($returnData);
	die();
}
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Перенарезка картинок</title>
  </head>
  <body class="p-5">
	  
	<h1>Настройки обработки картинок</h1>
	<div id="cacheAlert" class="alert alert-warning alert-dismissible fade show mb-5" role="alert" style="display: none;">
		<div id="alertMessage"></div>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div id="logInfo" class="mb-5" style="max-height: 300px; overflow: auto;"></div>

	<form id="resizeImageForm" class="mb-5">
		<div class="form-group">
			<label for="folder">Папка</label>
			<input type="text" class="form-control" name="folder" id="folder" aria-describedby="folderHelp" placeholder="введите папку" value="<?=$_COOKIE['folder']?>">
			<small id="folderHelp" class="form-text text-muted">Формат от корня сайта, пример: "/upload/iblock"</small>
		</div>
		<div class="form-group">
			<label for="quality">Качество</label>
			<input type="text" class="form-control" name="quality" id="quality" placeholder="100" value="<?=($_COOKIE['quality']?$_COOKIE['quality']:70)?>">
		</div>
		<div class="row">
			<div class="col">
				<div class="form-group">
					<label for="maxwidth">Максимальная ширина картинки</label>
					<input type="text" class="form-control" name="maxwidth" id="maxwidth" placeholder="1500" value="<?=($_COOKIE['maxwidth']?$_COOKIE['maxwidth']:1500)?>">
				</div>
			</div>
			<div class="col">
				<div class="form-group">
					<label for="maxheight">Максимальная высота картинки</label>
					<input type="text" class="form-control" name="maxheight" id="maxheight" placeholder="1500" value="<?=($_COOKIE['maxheight']?$_COOKIE['maxheight']:1500)?>">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="extension">Расширения файлов</label>
			<input type="text" class="form-control" name="extension" id="extension" aria-describedby="extensionHelp"  value="<?=($_COOKIE['extension']?$_COOKIE['extension']:'jpg,JPG,jpeg,JPEG,png,PNG,gif,GIF')?>">
			<small id="extensionHelp" class="form-text text-muted">Через запятую, пример: "jpg,JPG,jpeg,JPEG,png,PNG,gif,GIF"</small>
		</div>
		<div class="form-check mb-4">
			<input type="checkbox" class="form-check-input" name="resize" value="1" id="resize" checked>
			<label class="form-check-label" aria-describedby="resizeHelp" for="resize">Обрезать изображения</label>
			<small id="resizeHelp" class="form-text text-muted">Если не отмечено, только ужимает качество</small>
		</div>
		<div class="form-check mb-4">
			<input type="checkbox" class="form-check-input" name="forcestop" value="1" id="forcestop">
			<label class="form-check-label" aria-describedby="forcestopHelp" for="forcestop">Остановить обработку</label>
		</div>
		<input type="hidden" name="start-compress" value="1">
		<button id="sumbitButton" type="submit" class="btn btn-primary">Погнали!</button>
	</form>

	<button id="clearCache" type="submit" class="btn btn-light">Очистить кэш директорий</button>
	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  
	<script>
		$(function () {

			function sendForm(){
				$form = $('#resizeImageForm');
				$.ajax({
					type: 'POST',
					url: '/compres_image.php',
					data: $form.serialize(),
					success: function (data) {
						// console.log(data);
						var oData = JSON.parse(data);
						console.log(oData);

						if(oData.alertMessage){
							$('#alertMessage').text(oData.alertMessage);
							$('#cacheAlert').fadeIn();
						}
						if(oData.continue){
							$('#logInfo').html(oData.log);
							sendForm();
						}else{
							$('#sumbitButton').removeClass('disabled').removeAttr('disabled');
							clearCache();
						}
					},
					error: function () {
						console.log('Произошла ошибка!');
						sendForm();
					}
				});
			}

			function clearCache(){
				$.ajax({
					type: 'POST',
					url: '/compres_image.php',
					data: {clearcache:1},
					success: function (data) {
						var oData = JSON.parse(data);
						$('#alertMessage').text(oData.alertMessage);
						$('#cacheAlert').fadeIn();
					},
					error: function () {
						console.log('Произошла ошибка!');
					}
				});
			}
			
			$('#sumbitButton').click(function (e) { 
				e.preventDefault();
				$('#logInfo').html('');
				$(this).addClass('disabled').attr('disabled','disabled');
				sendForm();
			});

			$('#clearCache').click(function (e) { 
				e.preventDefault();
				clearCache();
			});


		});
	</script>

	</body>
</html>
